#Expose servcie via LoadBanlancer, access the servie "curl  externalip:80"
#kubectl expose deployment my-app-new --name my-app80  --type=LoadBalancer --port=80 --target-port=8080

#Expose service via NodePort, access the servcie "curl oneofworkernodeIP:NodePort"
kubectl expose deployment my-app-new --name my-app-node --type=NodePort --target-port=8080 --port=80
