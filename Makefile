DOCKNAME = registry.gitlab.com/consultron/kubelunch
DOCKVERSION = 0.3

build:
	docker build --rm=true --no-cache=true -t $(DOCKNAME):$(DOCKVERSION) .
push:
	docker push $(DOCKNAME):$(DOCKVERSION)
