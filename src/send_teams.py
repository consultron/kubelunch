#!/usr/bin/env python

from pymsteams import connectorcard, cardsection
import os

channel = os.getenv('CHANNEL')
c_link = 'http://157.230.79.110:8080/'
c_msg = 'This could be a lunch suggestion'

tmsg = connectorcard(channel)
tmsg.title('Hello from Kubelunch')
tmsg.text("This is a test message from send_teams.py!")
tsect = cardsection()
tsect.text(c_msg)
tmsg.addLinkButton('Visit the web page', c_link)
tmsg.addSection(tsect)
tmsg.send()
