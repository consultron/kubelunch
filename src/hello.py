from flask import Flask, render_template, request

import random
import datetime

import os
from pymsteams import connectorcard, cardsection


def lunch():

    MYDIR = os.path.dirname(__file__)
    lines = open(os.path.join(MYDIR, 'mall.txt')).read().splitlines()
    mychoice = random.choice(lines)
    today = datetime.datetime.now()

    if mychoice in ["Vapiano", "Drama"]:
        mychoice = random.choice(lines)
    

    return mychoice


def send_teams(txt):
    channel = os.getenv('CHANNEL', 'https://outlook.office.com/webhook/79234ee5-9100-4889-ad78-e98fdc29887e@05764a73-8c6f-4538-83cd-413f1e1b5665/IncomingWebhook/a341536cac834231b3839feb7d616b62/01ed606e-dfe4-4d2f-b0e4-6caefe1ebd53')
    c_msg = txt

    tmsg = connectorcard(channel)
    tmsg.title('Message from Kubelunch robot')
    tmsg.text("This is the message:")
    tsect = cardsection()
    tsect.text(c_msg)
    tmsg.addSection(tsect)
    tmsg.send()


app = Flask(__name__)


@app.route('/')
def hello():
    mychoice = lunch()
    today = datetime.datetime.now()
    client_ip = request.remote_addr
    msg = "Hello Today " + today.strftime('%Y-%m-%d') + " Lunch @" + mychoice + " from IP:" + client_ip
    send_teams(msg)
    kubelunch = {'lunch': mychoice, 'clientip':client_ip}
    return render_template('index.html', kubelunch=kubelunch)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080, debug=True)
