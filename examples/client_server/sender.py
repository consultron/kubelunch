#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# The router connects to the receiver to route messages to
# the outside.

import socket
from common import send_msg, recv_msg
from time import sleep
import sys
import traceback
import errno

HOST = '127.0.0.1'
PORT = 6014

try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(1)
    sleep(1)
    s.connect((HOST, PORT))
    send_msg(s, 'Hello world')
    msg = recv_msg(s)
    print('Got back: %s' % msg)
except socket.error as e:
    if e.errno == errno.ECONNREFUSED:
        print('Connection refused, try again!')
        sys.exit(1)
    else:
        print(e)
except KeyboardInterrupt:
    print('Keyint')
    sys.exit(0)
except BaseException:
    traceback.print_exc(file=sys.stdout)
    sleep(1)
