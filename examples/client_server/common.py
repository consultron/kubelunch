import struct
import traceback
import sys
import socket
import errno

RECVBUF = 1

msgcnt = 0


def send_msg(sock, msg):
    global msgcnt
    msgcnt += 1
    try:
        msg = struct.pack('>I', len(msg)) + msg
        sock.sendall(msg)

    except KeyboardInterrupt:
        raise

    except BaseException:
        traceback.print_exc(file=sys.stdout)
        raise
        

def recv_msg(sock):
    try:
        msg = ''
        tmout = 0
        while len(msg) != 4:
            if tmout == 10:
                print('Timeout exceeded')
                return None
            try:
                msg += sock.recv(RECVBUF)

            except socket.timeout as e:
                tmout += 1
                print('in timeout')
                pass
            except socket.error as e:
                if e.errno == errno.EAGAIN:
                    tmout += 1
                if e.errno == errno.ECONNRESET:
                    print('Connection reset!')
                    raise
            except BaseException:
                traceback.print_exc(file=sys.stdout)
                raise

        msglen = struct.unpack('>I', msg[0:4])[0]
        msg = msg[4:]

        tmout = 0
        while len(msg) != msglen:
            if tmout == 10:
                print('Timeout exceeded')
                return None

            try:
                msg += sock.recv(RECVBUF)

            except socket.timeout as e:
                print('in timeout')
                pass
            except socket.error as e:
                if not e.errno == errno.EAGAIN:
                    print(e)
            except BaseException:
                traceback.print_exc(file=sys.stdout)
                raise
        return msg

    except KeyboardInterrupt:
        raise

    except BaseException:
        traceback.print_exc(file=sys.stdout)
