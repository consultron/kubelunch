#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# The receiver is accepting messages and sends them to the router.

import select
import socket
import threading
import Queue
import traceback
from common import send_msg, recv_msg
from time import sleep
import sys


HOST = '127.0.0.1'
RCVPORT = 6014


class receiverThread(threading.Thread):
    def __init__(self, q):
        threading.Thread.__init__(self)
        self.queue = q
        self._stop_event = threading.Event()

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()

    def run(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind((HOST, RCVPORT))
        s.listen(1)
        s.settimeout(1)
        while not self.stopped():
            try:
                conn, addr = s.accept()

                print('Receiver got a client')
                msg = recv_msg(conn)
                send_msg(conn, 'OK')
                print('Receiver got %s ' % msg)
                conn.close()

            except socket.timeout:
                pass

            except BaseException:
                traceback.print_exc(file=sys.stdout)


class routingThread(threading.Thread):
    def __init__(self, q):
        threading.Thread.__init__(self)
        self.queue = q
        self._stop_event = threading.Event()

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()

    def run(self):

        while not self.stopped():
            try:
                while not self.stopped():
                    if not q.empty():
                        msg = q.get()
                        print('Got a message')

            except BaseException:
                traceback.print_exc(file=sys.stdout)

q = Queue.Queue()

thr_routing = routingThread(q)
thr_routing.start()

thr_rcv = receiverThread(q)
thr_rcv.start()

try:
    while True:
        sleep(3600)

except KeyboardInterrupt:
    print('Exiting...')


thr_routing.stop()
thr_rcv.stop()

thr_routing.join
print('Joined thread routing')
thr_rcv.join()
print('Joined thread receiver')

print('Bye, bye')
