FROM python:3.5-alpine

EXPOSE 8080

RUN mkdir -p /usr/src/app/templates
WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt

COPY src/hello.py src/mall.txt  /usr/src/app/
COPY src/templates/ /usr/src/app/templates

CMD [ "python", "hello.py"]
