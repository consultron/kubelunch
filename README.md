# kubelunch

Try out micro services on Kubernetes by making a simple lunch picker app.

# How to run the code localy
- Clone the repo
```
git clone git@gitlab.com:consultron/kubelunch.git
```
- Install pipenv if you do not have it and create a virtual environment
```
pip install pipenv --user
pipenv install -r requirements.txt
pipenv shell
```
- Run the hello code and unit test
```
python -m pytest tests/unit/
python -m pytest tests/unit/
curl http://127.0.0.1:8080/
```
- Build the image and push the image to GitLab registry
```
make build
make push
```
# How to deploy the application to k8 cluster
- Deploy the app
```
kubectl run my-app --replicas=2 --image=registry.gitlab.com/consultron/kubelunch:tag --port=p1
kubectl expose deployment my-app --type=LoadBalancer --port=p1 --target-port=p2
kubectl get service
```
- Test the app
```
curl [publicip]:[p2]
```
- Roll update
```
$kubectl set image deployment/my-app my-app=registry.gitlab.com/consultron/kubelunch:newtag
```
- Cleanup
```
kubectl delete deployment my-app
kubectl delete svc  my-app
```