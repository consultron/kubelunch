from src.hello import lunch
from hamcrest import assert_that, equal_to, starts_with, anything


class TestHello:
    def test_lunch(self):
        result = lunch()
        print ("result=%s" %result)
        assert_that(result, anything)
